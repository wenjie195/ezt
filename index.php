<?php
if (session_id() == ""){
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $referrerUID = $_SESSION['uid'];

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://mongroup.co/" />
    <link rel="canonical" href="https://mongroup.co/" />
    <meta property="og:title" content="Login | MON" />
    <title>Login | MON</title>
    
	<?php include 'css.php'; ?>
</head>
<body class="body">
<style>
.header1, .footer-div, .ow-blue-button1{
background: black;

	}
.ow-blue-button1{	
    background: #a37900;
	transition:.15s ease-in-out;}
.ow-blue-button1:hover{
    background: #8a6700;	
}
::placeholder {
	color:white !Important;
}
</style>
<?php include 'header.php'; ?>
<div class="width100 home-same-padding menu-distance darkbg blue-bg-ow min-height text-center"  id="firefly">
    
   <div class="index-login-width">
    
		
		<img src="img/white-logo.png" alt="<?php echo _JS_LOGIN ?>" title="<?php echo _JS_LOGIN ?>"   class="index-logo">
    	<h1 class="h1-title"><?php echo _JS_LOGIN ?></h1>
        <form action="utilities/loginFunction.php" method="POST">
            <input class="clean pop-input" type="text" placeholder="<?php echo _MAINJS_INDEX_USERNAME ?>" id="username" name="username" required>
            
            <div class="fake-input-bg">
                <input class="clean pop-input  no-bg-input" type="password" placeholder="<?php echo _MAINJS_INDEX_PASSWORD ?>" id="password" name="password" required>
                <img src="img/eye-white.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionD()" alt="View Password" title="View Password">
            </div>
            <div class="clear"></div>
            <button class="clean width100 blue-button white-text   ow-blue-button1" name="loginButton"><?php echo _JS_LOGIN ?></button>
            <p class="width100 text-center">	
                <a href="forgotPassword.php" class="blue-link forgot-a opacity-hover"><?php echo _JS_FORGOT_TITLE ?></a>
            </p>
          
            <div class="clear"></div>
        </form>
	</div>
    
    	


</div>
<?php include 'js.php'; ?>

</body>
</html>