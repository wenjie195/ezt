<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/MonthlyProfitBonus.php';
require_once dirname(__FILE__) . '/classes/DailyBonus.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/MpIdData.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $uid = $_SESSION['uid'];

$conn = connDB();

$dateCreated = rewrite($_POST['dateStart']);
$dateEnd = rewrite($_POST['dateEnd']);

if ($dateCreated) {
  $dateNew = str_replace("/","-",$dateCreated);
  $dateCreatedMin = date('Y-m-d',strtotime($dateNew));
}else {
  $dateCreated = "01/01/1970";
  $dateNew = str_replace("/","-",$dateCreated);
  $dateCreatedMin = date('Y-m-d',strtotime($dateNew));
}

if ($dateEnd) {
  $dateEndNew = str_replace("/","-",$dateEnd);
  $dateEndMin = date('Y-m-d',strtotime($dateEndNew));
  $dateCreatedMax = date('Y-m-d',strtotime($dateEndMin. "+ 1 day" ));
}else {
  $dateEndMin = date('Y-m-d');
  $dateCreatedMax = date('Y-m-d',strtotime($dateEndMin. "+ 1 day" ));
}

// $dailyBonusDetails = getDailyBonus($conn, "ORDER BY date_created DESC");
// $dailyBonusDetailsDaily = getDailyBonus($conn, "WHERE display = 1");
// $dailyBonusDetailsDailyUnreleased = getDailyBonus($conn, "WHERE display = 0");
$dailyBonusDetails = getDailyBonus($conn, "WHERE date_created >= ? and date_created < ?",array("date_created,date_created"),array($dateCreatedMin,$dateCreatedMax), "ss");
$dailyBonusDetailsDaily = getDailyBonus($conn, "WHERE date_created >= ? and date_created < ? and display = 1", array("date_created,date_created"), array($dateCreatedMin,$dateCreatedMax), "ss");
$dailyBonusDetailsDailyUnreleased = getDailyBonus($conn, "WHERE date_created >= ? and date_created < ? and display = 0", array("date_created,date_created"), array($dateCreatedMin,$dateCreatedMax), "ss");
$totalBon = 0;
$totalBonReleased = 0;
$totalBonUnreleased = 0;

$a = 1;

if ($dailyBonusDetails) {
  for ($m=0; $m <count($dailyBonusDetails) ; $m++) {
    $bonus = $dailyBonusDetails[$m]->getBonus();
    $totalBon += $bonus;
  }
}else {
  $totalBon = 0;
}
if ($dailyBonusDetailsDaily) {
  for ($m=0; $m <count($dailyBonusDetailsDaily) ; $m++) {
    $bonusReleased = $dailyBonusDetailsDaily[$m]->getBonus();
    $totalBonReleased += $bonusReleased;
  }
}else {
  $totalBonReleased = 0;
}
if ($dailyBonusDetailsDailyUnreleased) {
  for ($m=0; $m <count($dailyBonusDetailsDailyUnreleased) ; $m++) {
    $bonusUnreleased = $dailyBonusDetailsDailyUnreleased[$m]->getBonus();
    $totalBonUnreleased += $bonusUnreleased;
  }
}else {
  $totalBonUnreleased = 0;
}
$totalBonusReleased = number_format($totalBonReleased,4);
$totalBonusUnreleased = number_format($totalBonUnreleased,4);
$totalBonus = number_format($totalBon,4);

if ($dailyBonusDetails) {
  for ($cnt=0; $cnt <count($dailyBonusDetails) ; $cnt++) {
    $username = $dailyBonusDetails[$cnt]->getUsername();
    $from = $dailyBonusDetails[$cnt]->getFromWho();
    $bonus = $dailyBonusDetails[$cnt]->getBonus();
    $display = $dailyBonusDetails[$cnt]->getDisplay();
    $date = date('d/m/Y',strtotime($dailyBonusDetails[$cnt]->getDateCreated()));
    $time = date('h:i a',strtotime($dailyBonusDetails[$cnt]->getDateCreated()));

    $totalPayout[] = array("totalPayout" => $totalBonus, "totalPayoutReleased" => $totalBonusReleased, "totalPayoutUnreleased" => $totalBonusUnreleased, "date" => $dateCreatedMax, "username" => $username,
                          "from" => $from, "bonus" => $bonus, "display" => $display, "dateCreated" => $date, "timeCreated" => $time);
  }
}


echo json_encode($totalPayout);
 ?>
