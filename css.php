<link rel="shortcut icon" href="https://mongroup.co/img/ezt-favicon.ico" type="image/x-icon">
<link rel="icon" href="https://mongroup.co/img/ezt-favicon.ico" type="image/x-icon">
<!--<link rel="preconnect" href="https://fonts.gstatic.com">-->
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" >

<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/component.css?version=1.0.1">
<link rel="stylesheet" type="text/css" href="css/normalize.css">
<link rel="stylesheet" type="text/css" href="css/animate.css">
<link rel="stylesheet" type="text/css" href="css/upload-css.css?version=1.0.2">
<link rel="stylesheet" type="text/css" href="css/style.css?version=1.2.9">
