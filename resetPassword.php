<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = null;
$userRows = null;
$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>

<meta property="og:url" content="https://mongroup.co/resetPassword.php" />
<link rel="canonical" href="https://mongroup.co/resetPassword.php" />
<meta property="og:title" content=">Reset Password  | MON" />
<title>Reset Password  | MON</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<style>
.header1, .footer-div, .ow-blue-button1{
background: black;

	}
.ow-blue-button1{	
    background: #a37900;
	transition:.15s ease-in-out;}
.ow-blue-button1:hover{
    background: #8a6700;	
}
::placeholder {
	color:white !Important;
}
</style>
<?php include 'header.php'; ?>
<div class="width100 home-same-padding menu-distance darkbg min-height text-center blue-bg-ow" id="firefly">
    
    <div class="index-login-width">
		<img src="img/white-logo.png" alt="<?php echo _JS_RESET_PASSWORD ?>" title="<?php echo _JS_RESET_PASSWORD ?>"   class="index-logo">
    	<h1 class="h1-title"><?php echo _JS_RESET_PASSWORD ?></h1>
        <form action="utilities/resetPasswordFunction.php" method="POST">
            <input type="hidden" name="checkThat" value="<?php if(isset($_GET['uid'])){echo $_GET['uid'] ;}?>">
           

            <div class="fake-input-bg">
                <input class="clean pop-input  no-bg-input" type="text" placeholder="<?php echo _JS_VERIFY_CODE ?>" id="verify_code" name="verify_code" required>
            </div>
            
            <div class="clear"></div>

            <div class="fake-input-bg">
                <input class="clean pop-input  no-bg-input" type="password" placeholder="<?php echo _JS_NEW_PASSWORD ?>" id="new_password" name="new_password" required>
                <img src="img/eye-white.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">
            </div>

            <div class="clear"></div>

            <div class="fake-input-bg">
                <input class="clean pop-input  no-bg-input" type="password" placeholder="<?php echo _JS_RETYPE_NEW_PASSWORD ?>" id="retype_new_password" name="retype_new_password" required>
                <img src="img/eye-white.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="View Password" title="View Password">
            </div>

            <div class="clear"></div>

            <button class="clean width100 blue-button white-text   ow-blue-button1" name="submit"><?php echo _JS_SUBMIT ?></button>
          
            <div class="clear"></div>
        </form>


    </div>   


</div>


<?php include 'js.php'; ?>

<script>
function myFunctionA()
{
    var x = document.getElementById("new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionB()
{
    var x = document.getElementById("retype_new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>

</body>
</html>