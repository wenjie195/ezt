<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/MpIdData.php';
require_once dirname(__FILE__) . '/classes/EquityPlRawData.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

require_once dirname(__FILE__) . '/helper/Helper.php';


/****************************************************  PROCESS  *******************************************************/

$conn = connDB();

$users = getUser($conn,"WHERE username != 'admin' and username != 'infinox'");

foreach ($users as $user) {
  $uid = $user->getUid();
  $downlineReferralHistory = getReferralHistory($conn,"WHERE referral_id =?",array("referral_id"),array($uid),"s"); // get referral history
  $total_direct_referral = 0;
  
  if ($downlineReferralHistory) {
    $referrer_level = $downlineReferralHistory[0]->getCurrentLevel();
    $downline_calculation[$uid] = downline_calculation($conn,$uid,$referrer_level,$total_direct_referral); // call funtion downline_calculation()
    $total_direct_referral = $downline_calculation[$uid]['total_direct_referral'];
    $group_sales = $downline_calculation[$uid]['group_sales'];
    $aa = $downline_calculation[$uid]['indirect_arr'];
  }
}
$identify_rank = identify_rank($conn,$downline_calculation); // call function identify_rank()
update_rank($conn,$identify_rank); // call function update_rank()