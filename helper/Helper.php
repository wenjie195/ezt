<?php

function user_details($conn,$uid){

    $data_info['users'] = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
    $data_info['ranks'] = rank_detail($conn,$data_info['users'][0]->getRankId());
    $data['user'] = $data_info['users'][0];
    $data['rank'] = $data_info['ranks']['rank'];
    return $data;
}

function rank_detail($conn,$rankId){

    $sql = 'SELECT cr.rank, cr.bonus ';
    $sql .= 'FROM user u ';
    $sql .= 'INNER JOIN cmn_rank cr ON cr.rank_id = u.rank_id ';
    $sql .= 'WHERE u.rank_id = '.$rankId;
    // echo $sql;
    // die;
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        return $row;
    }
}

function getBonus($conn,$uid,$username,$fromWho,$bonus){

     if(insertDynamicData($conn,"daily_bonus",array("uid", "username","from_who","bonus"),
          array($uid,$username,$fromWho,$bonus),"ssss") === null)
     {
          // echo "gg";
     }
     else
     {    }
     return true;
}

function user_balance($conn,$uid){

    $userBalance = 0;
    $userMpIdDataDetails = getMpIdData($conn,"WHERE uid = ?",array("uid"),array($uid), "s");
        if ($userMpIdDataDetails) {
            for ($cntx=0; $cntx <count($userMpIdDataDetails) ; $cntx++) {
                $userBalance += $userMpIdDataDetails[$cntx]->getBalance();
            }
        }else {
            $userBalance = 0;
        }

        return $userBalance;
}

function claim_status($current_claim,$total_claim,$next_claim){

    if ($total_claim <= 15) {
        $status = 'OK';
    }else{
        $status = 'EXCEED';
    }

    if ($status == 'EXCEED') {
        $next_claim = 15 - $current_claim;
      }

    return $next_claim;
}

function daily_bonus_calculate($conn,$total_claim,$upline_rank,$current_level,$user_balance){

  $companyBalance = $this->company_balance();
  $member_lot = $user_balance * $companyBalance;

  isset($current_level) && is_array($current_level) ? rsort($current_level) : '';

  if ($upline_rank > $current_level[0]) { // compare current upline rank to old upline rank
    $rank_detail = rank_detail($conn,$upline_rank);
    $next_claim = ($rank_detail['bonus'] * $member_lot) - $total_claim;
    $current_claim = $total_claim;
    $total_claim += ($rank_detail['bonus'] * $member_lot);
    $data['total_claim'] = $total_claim;
    $data['bonus_claim'] = claim_status($current_claim,$total_claim,$next_claim);

  //   echo $data['bonus_claim'].'<br>';

  }else{
      $data['total_claim'] = $total_claim;
      $data['bonus_claim'] = 0;
  }
  return $data;
}

function existed_daily_bonus_today($conn,$today_date){

    $daily_bonus_detail = getDailyBonus($conn,'WHERE date_created = ?',array('date_created'), array($today_date), 's');

    if (isset($daily_bonus_detail)) {
        $status = 'EXIST';
    }else{
        $status = 'NONE';
    }

    return $status;
}

function rank_list($conn){

    $sql = 'SELECT rank_id, rank, bonus ';
    $sql .= 'FROM cmn_rank ';
    
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $data['rank_id'][] = $row['rank_id'];
        }
        // print_r($data);
        // die;
        return $data;
    }
}

/****************************************************  rankIdentify.php  *******************************************************/

function personal_sales($conn,$uid){ // calculate personal sales
    $personalSales = 0;
    $mpIdData = getMpIdData($conn,"WHERE uid =?",array("uid"),array($uid),"s");
    if ($mpIdData) {
      for ($a=0; $a <count($mpIdData) ; $a++) {
        $personalSales += $mpIdData[$a]->getBalance();
      }
    }
  
    return $personalSales;
  }
  
  function downline_calculation($conn,$uid,$referrer_level,$total_direct_referral){ // get personal, group sales, total_direct_referrral, indirect_referral
  
    $data['uid'] = $uid;
    $data['personal_sales'] = personal_sales($conn,$uid);
    $data['group_sales'] = 0;
    $data['total_direct_referral'] = 0;
    $data['total_indirect'] = 0;
    $data['total_referral'] = 0;
  
    $getWho = getWholeDownlineTree($conn, $uid,false); // get downline user
    $data['indirect_arr'] = indirect_referral($conn,$uid,$getWho,$referrer_level);
    $data['total_indirect'] = isset($data['indirect_arr']) ? array_sum($data['indirect_arr']) : 0;
    
    if ($getWho) {
      
      foreach ($getWho as $downline) {
        $data['group_sales'] = group_sales($conn,$downline->getReferralId(),$data['group_sales']);
        $referral_det['referral_details'] = referral_details($conn,$downline->getReferralId(),$referrer_level,$total_direct_referral);
        $total_direct_referral += $referral_det['referral_details']['total_direct_referral'];
        $data['total_direct_referral'] = $total_direct_referral;
      }
      $data['total_referral'] = $data['total_direct_referral'] + $data['total_indirect'];
    }
      return $data;
  }
  
  function group_sales($conn,$uid,$downlinePersonalSales=0){
  
    $downlineReferralHistory = getReferralHistory($conn,"WHERE referral_id =?",array("referral_id"),array($uid),"s"); // get referral history
    $uid = $downlineReferralHistory[0]->getReferralId();
  
    $mpIdDataGroup = getMpIdData($conn,"WHERE uid =?",array("uid"),array($uid),"s");
    if (isset($mpIdDataGroup)) {
      for ($b=0; $b <count($mpIdDataGroup) ; $b++) {
        $downlinePersonalSales += $mpIdDataGroup[$b]->getBalance();
      }
    }else{
      $downlinePersonalSales += 0;
    }
  
    return $downlinePersonalSales;
  }
  
  function referral_details($conn,$uid,$referrer_level,$total_direct_referral){
  
    $downlineReferralHistory = getReferralHistory($conn,"WHERE referral_id =?",array("referral_id"),array($uid),"s"); // get referral history
    $uid = $downlineReferralHistory[0]->getReferralId();
    $referral_level = $downlineReferralHistory[0]->getCurrentLevel();
    $data['total_direct_referral'] = direct_referral($conn,$uid,$referral_level,$referrer_level,$total_direct_referral);
  
    return $data;
  }
  
  function direct_referral($conn,$uid,$level,$referrer_level,$total_direct_referral){
  
    $direct_referral = $referrer_level + 1;
    if ($level == $direct_referral) {
      $total_direct_referral = 1;
    }else{
      $total_direct_referral = 0;
    }
  
    return $total_direct_referral;
  }
  
  function indirect_referral($conn,$uid,$getWho,$referrer_level){
  
    $getWho = getWholeDownlineTree($conn, $uid,false); // get downline user
    $ranks = rank_list($conn);
    $direct_level = $referrer_level + 1;
  
    foreach ($ranks['rank_id'] as $rank) {
        $data[$rank] = 0;
    }
  
    if ($getWho) {
    
      foreach ($getWho as $indirect) {
        $referral_history = getReferralHistory($conn, 'WHERE referral_id =?',array('referral_id'),array($indirect->getReferralId()), 's');
        $referral_level = $referral_history[0]->getCurrentLevel();
        if ($direct_level != $referral_level) {
          foreach ($ranks['rank_id'] as $rank) {
  
            $data[$rank] = isset($data[$rank]) ? $data[$rank] : 0;
    
            $sql = 'SELECT COUNT(u.id) AS referral_'.$rank;
            $sql .= ' FROM user u';
            $sql .= ' INNER JOIN referral_history rh ON rh.referral_id = u.uid';
            $sql .= ' WHERE rh.referral_id = "'.$indirect->getReferralId().'"';
            $sql .= ' AND rank_id ='.$rank;
            $sql .= ' ORDER BY COUNT(u.id) DESC';
    
            $result = $conn->query($sql);
              if ($result->num_rows > 0) {
                  $row = $result->fetch_assoc();
              }
              $data[$rank] += $row['referral_'.$rank];
          }
        }
      }
    }
  
      return $data;
  }
  
  function identify_rank($conn,$downline_arr){
  
    foreach ($downline_arr as $key => $person) {
      $identify_rank = cmn_rank($conn,$person['personal_sales'],$person['group_sales'],$person['total_referral'],$person['indirect_arr']);
      $downline_arr[$key]['rank_updated'] = isset($identify_rank) ? $identify_rank : 1;
    }
    return $downline_arr;
  }
  
  function cmn_rank($conn,$self_invest,$group_sales,$total_referral,$referrals){
    // print_r($referrals);
    unset($data);
    $data = [];
  
    foreach ($referrals as $key => $referral) {
      $sql = ' SELECT rank_id';
      $sql .= ' FROM cmn_rank';
      $sql .= ' WHERE self_invest <= '.$self_invest;
      $sql .= ' AND group_sales <= '.$group_sales;
      $sql .= ' AND (referral_rank_id = '.$key.' OR referral_rank_id is NULL)';
      $sql .= ' AND (direct_no <= '.$total_referral.' OR direct_no is NULL)';
      $sql .= ' AND (indirect_no <= '.$referral.' OR indirect_no is NULL)';
      $sql .= ' ORDER BY rank_id DESC';
      $sql .= ' LIMIT 1';
      $result = $conn->query($sql);
      // echo $sql;
  
      if ($result && $result->num_rows > 0) {
          $row = $result->fetch_assoc();
          $data[] = $row['rank_id'];
      }
    }
    if (isset($data)) {
      rsort($data);
      return $data[0];
    }
  }
  
  function update_rank($conn,$data_arr){
    print_r($data_arr);
  
    foreach ($data_arr as $key => $data) {
      
      $sql = 'UPDATE user';
      $sql .= ' SET rank_id = '.$data['rank_updated'];
      $sql .= ' WHERE uid = \''.$data['uid'].'\'';
  
      $result = $conn->query($sql);
    }
    return;
  }

  function company_balance(){
    $companyBalance = getCompanyBalance($conn);
    $ctb = $companyBalance[0]->getCtb();
    $tls = $companyBalance[0]->getTls();
    $data = $tls / $ctb;

    return $data;
  }
?>