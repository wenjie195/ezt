<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/MpIdData.php';
require_once dirname(__FILE__) . '/../classes/DailyBonus.php';
require_once dirname(__FILE__) . '/../classes/MonthlyBonus.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/languageFunction.php';

$conn = connDB();
$cnt = 0;
$success = '';
$resetBtn = 0;
$resetBtn = date('Y-m-d');

$dateCreated = $resetBtn;
$dailyBonusDetails = getDailyBonus($conn,"WHERE date_created >=?", array("date_created"), array($dateCreated), "s");


  if ($dailyBonusDetails) {
    for ($i=0; $i <count($dailyBonusDetails) ; $i++) {
      $id = $dailyBonusDetails[$i]->getId();

      $sql = "DELETE FROM daily_bonus WHERE id = $id";
      $conn->query($sql);
    }
    $data['return'] = 'Successfully reset daily spread.';
    echo json_encode($data);
  }else {
    $data['return'] = 'No daily spread for today to be reset.';
    echo json_encode($data);
  }
 ?>
