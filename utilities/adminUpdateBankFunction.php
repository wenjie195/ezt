<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $id = $_POST["user_uid"];

     $bankAccName = $_POST["update_bank_acc_name"];
     $bankAccNumber = $_POST["update_bank_acc_number"];
     $bankAccType = $_POST["update_bank_acc_type"];
     $bankName = $_POST["update_bank_name"];
     $bankSwiftCode = $_POST["update_bank_swift_code"];
     $bankCountry = $_POST["update_bank_country"];

     $userDetails = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");

     if(!$userDetails)
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($bankAccName)
          {
               array_push($tableName,"bank_acc_name");
               array_push($tableValue,$bankAccName);
               $stringType .=  "s";
          }
          if($bankAccNumber)
          {
               array_push($tableName,"bank_acc_number");
               array_push($tableValue,$bankAccNumber);
               $stringType .=  "s";
          }
          if($bankAccType)
          {
               array_push($tableName,"bank_acc_type");
               array_push($tableValue,$bankAccType);
               $stringType .=  "s";
          }
          if($bankName)
          {
               array_push($tableName,"bank_name");
               array_push($tableValue,$bankName);
               $stringType .=  "s";
          }
          if($bankSwiftCode)
          {
               array_push($tableName,"bank_swift_code");
               array_push($tableValue,$bankSwiftCode);
               $stringType .=  "s";
          }
          if($bankCountry)
          {
               array_push($tableName,"bank_country");
               array_push($tableValue,$bankCountry);
               $stringType .=  "s";
          }

          array_push($tableValue,$id);
          $stringType .=  "s";
          $updatedBankDetails = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($updatedBankDetails)
          {
               // echo "success";
               $_SESSION['messageType'] = 1;
               header('location: ../adminViewMember.php?type=3');
          }
          else
          {
            $_SESSION['messageType'] = 1;
            header('Location: ../adminViewMember.php?type=4');
          }
     }
     else
     {
       $_SESSION['messageType'] = 1;
       header('Location: ../adminViewMember.php?type=5');
     }
}
else
{
     header('Location: ../index.php');
}
?>
