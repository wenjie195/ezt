<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    // $uid = $_SESSION['uid'];

    // $firstname = rewrite($_POST['update_firstname']);
    // $lastname = rewrite($_POST["update_lastname"]);

    $uid = rewrite($_POST["user_uid"]);
    $register_password = rewrite($_POST["password"]);

    $password = hash('sha256',$register_password);
    $salt = substr(sha1(mt_rand()), 0, 100);
    $finalPassword = hash('sha256', $salt.$password);

    $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");

    if(!$user)
    {
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($finalPassword)
        {
            array_push($tableName,"password");
            array_push($tableValue,$finalPassword);
            $stringType .=  "s";
        }
        if($salt)
        {
            array_push($tableName,"salt");
            array_push($tableValue,$salt);
            $stringType .=  "s";
        }

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
          $_SESSION['messageType'] = 1;
          header('Location: ../adminViewMember.php?type=2');
        }
        else
        {
          $_SESSION['messageType'] = 1;
          header('Location: ../adminViewMember.php?type=4');
        }
    }
    else
    {
      $_SESSION['messageType'] = 1;
      header('Location: ../adminViewMember.php?type=5');
    }

}
else
{
    header('Location: ../index.php');
}
?>
