<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Status.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/languageFunction.php';

$senderUID = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $tz = 'Asia/Kuala_Lumpur';
     $timestamp = time();
     $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
     $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
     $time = $dt->format('Y-m-d H:i:s');

     if (isset($_POST['submitIcFront']))
     {
          $uid = $senderUID;
          //   $icfront = $_FILES['IcFront']['name'];
          $icfront = $timestamp.$_FILES['IcFront']['name'];
          $update = "2";

          $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
          $userUsername = $userDetails[0]->getUsername();
          $username = $userUsername;
          $icfrontTimeline = $time;

          // $target_dir = "../uploads/";
          $target_dir = "../uploads/";
          $target_file = $target_dir . basename($_FILES["IcFront"]["name"]);

          // Select file type
          $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
          // Valid file extensions
          $extensions_arr = array("jpg","jpeg","png","gif");

          if( in_array($imageFileType,$extensions_arr) )
          {
               move_uploaded_file($_FILES['IcFront']['tmp_name'],$target_dir.$icfront);
          }
          if($icfront == "")
          {
               header('Location: ../uploadFrontIC.php');
          }
          else
          {
               if(submitImage($conn,$uid,$username,$icfront,$icfrontTimeline))
               {
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    // if($icfront)
                    // {
                    //      array_push($tableName,"icfront_image");
                    //      array_push($tableValue,$icfront);
                    //      $stringType .=  "s";
                    // }
                    if($update)
                    {
                         array_push($tableName,"icfront");
                         array_push($tableValue,$update);
                         $stringType .=  "s";
                    }
                    array_push($tableValue,$uid);
                    $stringType .=  "s";
                    $messageStatusInUser = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($messageStatusInUser)
                    {
                         header('Location: ../uploadDoc.php');
                    }
                    else
                    {
                         header('Location: ../uploadDoc.php?type=2');
                    }
               }
               else
               {
               header('Location: ../uploadDoc.php?type=3');
               }
          }
     }

     if (isset($_POST['submitIcBack']))
     {
          $uid = $senderUID;
          //   $icback = $_FILES['icBack']['name'];
          $icback = $timestamp.$_FILES['icBack']['name'];
          $update = "2";

          $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
          $userUsername = $userDetails[0]->getUsername();
          $username = $userUsername;
          $icbackTimeline = $time;

          // $target_dir = "../uploads/";
          $target_dir = "../uploads/";
          $target_file = $target_dir . basename($_FILES["icBack"]["name"]);

          // Select file type
          $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
          // Valid file extensions
          $extensions_arr = array("jpg","jpeg","png","gif");

          if( in_array($imageFileType,$extensions_arr) )
          {
               move_uploaded_file($_FILES['icBack']['tmp_name'],$target_dir.$icback);
          }

          if($icback == "")
          {
               header('Location: ../uploadBackIC.php');
          }
          else
          {
               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               //echo "save to database";
               if($icback)
               {
                    array_push($tableName,"icback_image");
                    array_push($tableValue,$icback);
                    $stringType .=  "s";
               }
               if($icbackTimeline)
               {
                    array_push($tableName,"icback_timeline");
                    array_push($tableValue,$icbackTimeline);
                    $stringType .=  "s";
               }
               array_push($tableValue,$uid);
               $stringType .=  "s";
               $updatedIcBack = updateDynamicData($conn,"status"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
               if($updatedIcBack)
               {
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    // if($icback)
                    // {
                    //      array_push($tableName,"icback_image");
                    //      array_push($tableValue,$icback);
                    //      $stringType .=  "s";
                    // }
                    if($update)
                    {
                         array_push($tableName,"icback");
                         array_push($tableValue,$update);
                         $stringType .=  "s";
                    }
                    array_push($tableValue,$uid);
                    $stringType .=  "s";
                    $imageStatusInUser = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($imageStatusInUser)
                    {
                         header('Location: ../uploadDoc.php');
                    }
                    else
                    {
                         header('Location: ../uploadDoc.php?type=4');
                    }
               }
               else
               {
                    header('Location: ../uploadDoc.php?type=5');
               }
          }
     }

     if (isset($_POST['submitSignature'])) 
     {
          $uid = $senderUID;
          //   $signature = $_FILES['sign']['name'];
          $signature = $timestamp.$_FILES['sign']['name'];
          $update = "2";

          $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
          $userUsername = $userDetails[0]->getUsername();
          $username = $userUsername;
          $signatureTimeline = $time;

          // $target_dir = "../uploads/";
          $target_dir = "../uploads/";
          $target_file = $target_dir . basename($_FILES["sign"]["name"]);

          // Select file type
          $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
          // Valid file extensions
          $extensions_arr = array("jpg","jpeg","png","gif");

          if( in_array($imageFileType,$extensions_arr) )
          {
               move_uploaded_file($_FILES['sign']['tmp_name'],$target_dir.$signature);
          }
          if($signature == "")
          {
               header('Location: ../uploadSignature.php');
          }
          else
          {
               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               //echo "save to database";
               if($signature)
               {
                    array_push($tableName,"signature_image");
                    array_push($tableValue,$signature);
                    $stringType .=  "s";
               }
               if($signatureTimeline)
               {
                    array_push($tableName,"signature_timeline");
                    array_push($tableValue,$signatureTimeline);
                    $stringType .=  "s";
               }
               array_push($tableValue,$uid);
               $stringType .=  "s";
               $updatedLicense = updateDynamicData($conn,"status"," WHERE uid = ? ",$tableName,$tableValue,$stringType);

               if($updatedLicense)
               {
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    // if($signature)
                    // {
                    //      array_push($tableName,"signature_image");
                    //      array_push($tableValue,$signature);
                    //      $stringType .=  "s";
                    // }
                    if($update)
                    {
                         array_push($tableName,"signature");
                         array_push($tableValue,$update);
                         $stringType .=  "s";
                    }
                    array_push($tableValue,$uid);
                    $stringType .=  "s";
                    $imageStatusInUser = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($imageStatusInUser)
                    {
                         header('Location: ../uploadDoc.php');
                    }
                    else
                    {
                         header('Location: ../uploadDoc.php?type=6');
                    }
               }
               else
               {
                    header('Location: ../uploadDoc.php?type=7');
               }
          }
     }

     if (isset($_POST['submitLicense'])) 
     {
          $uid = $senderUID;
          //   $license = $_FILES['license']['name'];
          $license = $timestamp.$_FILES['license']['name'];
          $update = "2";

          $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
          $userUsername = $userDetails[0]->getUsername();
          $username = $userUsername;
          $licenseTimeline = $time;

          // $target_dir = "../uploads/";
          $target_dir = "../uploads/";
          $target_file = $target_dir . basename($_FILES["license"]["name"]);

          // Select file type
          $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
          // Valid file extensions
          $extensions_arr = array("jpg","jpeg","png","gif");

          if( in_array($imageFileType,$extensions_arr) )
          {
               move_uploaded_file($_FILES['license']['tmp_name'],$target_dir.$license);
          }
          if($license == "")
          {
          header('Location: ../uploadLicense.php');
          }
          else
          {
               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               //echo "save to database";
               if($license)
               {
                    array_push($tableName,"license_image");
                    array_push($tableValue,$license);
                    $stringType .=  "s";
               }
               if($licenseTimeline)
               {
                    array_push($tableName,"license_timeline");
                    array_push($tableValue,$licenseTimeline);
                    $stringType .=  "s";
               }
               array_push($tableValue,$uid);
               $stringType .=  "s";
               $updatedLicense = updateDynamicData($conn,"status"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
               if($updatedLicense)
               {
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    // if($license)
                    // {
                    //      array_push($tableName,"license_image");
                    //      array_push($tableValue,$license);
                    //      $stringType .=  "s";
                    // }
                    if($update)
                    {
                         array_push($tableName,"license");
                         array_push($tableValue,$update);
                         $stringType .=  "s";
                    }
                    array_push($tableValue,$uid);
                    $stringType .=  "s";
                    $imageStatusInUser = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($imageStatusInUser)
                    {
                         header('Location: ../uploadDoc.php');
                    }
                    else
                    {
                         header('Location: ../uploadDoc.php?type=8');
                    }
               }
               else
               {
                    header('Location: ../uploadDoc.php?type=9');
               }
          }
     }

}
else
{
     header('Location: ../index.php');
}

function submitImage($conn,$uid,$username,$icfront,$icfrontTimeline)
{
     if(insertDynamicData($conn,"status",array("uid","username","icfront_image","icfront_timeline"),
     array($uid,$username,$icfront,$icfrontTimeline),"ssss") === null)
     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}
?>