<?php
if (session_id() == ""){
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
	<meta property="og:url" content="https://mongroup.co/forgotPassword.php" />
    <link rel="canonical" href="https://mongroup.co/forgotPassword.php" /> 
    <meta property="og:title" content="<?php echo _JS_FORGOT_TITLE ?> | MON" />
    <title><?php echo _JS_FORGOT_TITLE ?> | MON</title>
    
	<?php include 'css.php'; ?>
</head>
<body class="body">
<style>
.header1, .footer-div, .ow-blue-button1{
background: black;	
	}
.ow-blue-button1{	
    background: #a37900;
	transition:.15s ease-in-out;}
.ow-blue-button1:hover{
    background: #8a6700;	
}
::placeholder {
	color:white !Important;
}
</style>
<?php include 'header.php'; ?>
<div class="width100 home-same-padding menu-distance darkbg min-height text-center blue-bg-ow" id="firefly">
    
    <div class="index-login-width">
    <img src="img/white-logo.png" alt="<?php echo _JS_FORGOT_TITLE ?>" title="<?php echo _JS_FORGOT_TITLE ?>"   class="index-logo">
    <h1 class="h1-title"><?php echo _JS_FORGOT_TITLE ?></h1>     
    
        <!-- <form action="" method="POST"> -->
        <form class="login-form" method="POST" action="utilities/forgotPasswordFunction.php">

            <input class="clean pop-input" type="text" placeholder="<?php echo _MAINJS_INDEX_USERNAME ?>" id="username" name="username" required>

            <div class="clear"></div>

            <input class="clean pop-input" type="email" placeholder="<?php echo _JS_EMAIL ?>" id="forgot_password" name="forgot_password" required>

            <div class="clear"></div>

            <button class="clean width100 blue-button   ow-blue-button1" name="loginButton"><?php echo _JS_SUBMIT ?></button>
            <p class="width100 text-center">	
                <a href="index.php" class="blue-link forgot-a opacity-hover"><?php echo _JS_LOGIN ?></a>
            </p>
          
            <div class="clear"></div>
            
        </form>
    </div>


</div>
<?php include 'js.php'; ?>
</body>
</html>