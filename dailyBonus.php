<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Status.php';
require_once dirname(__FILE__) . '/classes/CompanyBalance.php';
require_once dirname(__FILE__) . '/classes/MpIdData.php';
require_once dirname(__FILE__) . '/classes/EquityPlRawData.php';
require_once dirname(__FILE__) . '/classes/DailyBonus.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

require_once dirname(__FILE__) . '/helper/Helper.php';

$conn = connDB();
$today_date = date('Y-m-d');
$success = 'Successful run the process without any errors.';
$error = 'Existed. Reset daily bonus to re-run the daily bonus.';
$data['success'] = $success;
$data['error'] = $error;

$existed_bonus = existed_daily_bonus_today($conn,$today_date);
echo isset($existed_bonus) && $existed_bonus == 'NONE' ? daily_bonus_process($conn,$data) : error_process($data);

function daily_bonus_process($conn,$data){
  
  $users = getUser($conn, "WHERE username != 'admin' and username != 'company'");

  for ($k=0; $k < count($users) ; $k++) {
    $total_level_array = [];
    $bonus_data['total_claim'] = 0;
    $uid = $users[$k]->getUid();
    $fromWho = $users[$k]->getUsername();

    $data_store[$uid]['username'] = $fromWho;
    $user_balance = user_balance($conn,$uid);
    $data_store[$uid]['balance'] = $user_balance;

  $uplineDetails = getTop10ReferrerOfUser($conn,$uid); // get upline

  for ($count=0; $count <count($uplineDetails) ; $count++) {
    if (isset($uplineDetails[$count])) {

      $userUplineDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uplineDetails[$count]),"s");
      $username = $userUplineDetails[0]->getUsername();

      $data_store[$uid]['upline_name'][] = $userUplineDetails[0]->getUsername();
      $data_store[$uid]['upline_rank'][] = $userUplineDetails[0]->getRankId();

        $user_rank = $users[$k]->getRankId();
        $upline_rank = $userUplineDetails[0]->getRankId();
        $current_level = isset($total_level_array) && count($total_level_array) > 0 ? $total_level_array : 0; //store previous upline rank
        $bonus_data = daily_bonus_calculate($conn,$bonus_data['total_claim'],$upline_rank,$current_level,$user_balance);
        isset($bonus_data['bonus_claim']) && $bonus_data['bonus_claim'] != 0 ? getBonus($conn,$uid,$username,$fromWho,$bonus_data['bonus_claim']) : '';
        $total_level_array[] = $upline_rank;
        $data_store[$uid]['bonus_claim'][] = $bonus_data['bonus_claim'];
      }
    }
  }
  //   print_r($data_store);
  $return['return'] = $data['success'];

  echo json_encode($return);
}

function error_process($data){

    $return['return'] = $data['error'];
    echo json_encode($return);
}
 ?>
